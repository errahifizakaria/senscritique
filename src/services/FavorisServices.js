const FAVORIS = "favoris";

export const getFavorisFromLocalStorage = () => {
  return JSON.parse(localStorage.getItem(FAVORIS)) || [];
};

export const addFavorisToLocalStorage = character => {
  const tab = getFavorisFromLocalStorage();

  if (!Object.is(tab.id, character.id)) {
    var index = tab.findIndex(t => t.id===character.id)
    if (index === -1){
        tab.push(character);
    }else console.log("object already exists")

    localStorage.setItem(FAVORIS, JSON.stringify(tab));
  }
 
};

export const removeFavorisFromLocalStorage = id => {
  let tab = getFavorisFromLocalStorage();

  if (tab.find(t => t.id === id)) {
    tab = tab.filter(item => item.id !== id);
  }

  localStorage.setItem(FAVORIS, JSON.stringify(tab));
};
