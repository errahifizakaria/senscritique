import React from 'react';

import Contact from './components/contact/index'
import Home from './components/home/index'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Single from './components/single/index';




function App() {
  return (
    <Router>
<Switch>
  
<Route exact path='/' component={Home} />
<Route exact path='/contact' component={Contact} />
<Route exact path='/movie/:id' component={Single} />
</Switch>
</Router>
  );
}

export default App;
