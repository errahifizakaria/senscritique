import React, { useState, useEffect } from "react";
import styled from "styled-components";
import axios from "axios";
import Rater from "react-rater";
import logo from "../../assets/img/sprite.png";
import "react-rater/lib/react-rater.css";
import {
  getFavorisFromLocalStorage,
  addFavorisToLocalStorage,
  removeFavorisFromLocalStorage
} from "../../services/FavorisServices";
import { Heart } from "@styled-icons/octicons";

function Nowplaying() {
  const [data, setData] = useState([]);
  const [fav, setFav] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const res = await axios(
        "https://api.themoviedb.org/3/movie/now_playing?api_key=8bdfe857f0bc2c006c618bf788f71f61"
      );
      setData(res.data.results);
      console.log(res.data.results);
    };
    fetchData();
    //console.log( fetchData())
  }, []);
  
    const add = (movie) => {
     
      const dataFavories = getFavorisFromLocalStorage()
      addFavorisToLocalStorage(movie);
      setFav(!fav)
      if (Object.keys(dataFavories).length > 0) {
          //addFavorisToLocalStorage(movie);
          //setFav(addFavorisToLocalStorage(movie))

      }

    }
  const remove = id => {
   
    removeFavorisFromLocalStorage(id);
    setFav(!fav)
  };
  return (
    <WrapperItems>
      <Items>
        {data.slice(0, 4).map(movie => {
          return (
            <Item key={movie.id}>
              <ItemLink>
                <figure>
                    <WistHeart onClick={e => add(movie)} className="add_Fav"></WistHeart> 
                      <WistHeartDelete onClick={e => remove(movie.id)} className="delete_Fav"></WistHeartDelete>
                  
                  
                  <img
                    src={`https://image.tmdb.org/t/p/w300/${movie.poster_path}`}
                    alt={movie.title}
                  />
                  <RatingMovie> {movie.vote_average}</RatingMovie>
                </figure>
                <span>{movie.title}</span>
              </ItemLink>
              <RatingWrapper>
                <Rater
                  total={5}
                  rating={movie.vote_average / 2}
                  onRate={r => console.log(r.rating)}
                />
              </RatingWrapper>
            </Item>
          );
        })}
      </Items>
    </WrapperItems>
  );
}
const WistHeart = styled(Heart)`
  height: 16px;
  width: 16px;
  display: inline-block;
  position: absolute;
  right: 4px;
  top: 4px;
  cursor: pointer;
`;
const WistHeartDelete = styled(Heart)`
  height: 16px;
  width: 16px;
  display: inline-block;
  position: absolute;
  left: 4px;
  top: 4px;
  cursor: pointer;
  color:red;
`;
const WrapperItems = styled.div``;
const Items = styled.ul`
  padding: 0px;
  display: flex;
`;
const Item = styled.li`
  width: calc(25% - 10px);
  margin: 0 3px 15px;
  position: relative;
  vertical-align: bottom;
  display: inline-block;
`;

const ItemLink = styled.a`
  display: block;
  color: #333;
  figure {
    position: relative;
    margin: 0;
    margin-bottom: 15px;
    img {
      height: auto;
      width: 100%;
      border: 0;
      vertical-align: top;
    }
  }
  span {
    font-size: 12px;
    line-height: 18px;
    margin-bottom: 0;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    text-align: left;
    width: 100%;
    display: block;
    word-wrap: break-word;
  }
`;
const RatingMovie = styled.div`
  background: #222;
  color: #fff;
  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
  font-size: 16px;
  height: 26px;
  line-height: 26px;
  text-align: center;
`;
const RatingWrapper = styled.div`
  li {
    margin-right: 2px !important;
  }
`;
export default Nowplaying;
