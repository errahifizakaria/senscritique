import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { ArrowBack } from "@styled-icons/boxicons-regular/ArrowBack";
import styled from "styled-components";
import axios from "axios";
import Rater from "react-rater";
import logo from "../../assets/img/sprite.png";
import "react-rater/lib/react-rater.css";
const Single = () => {
  const [data, setData] = useState([]);

  const id = useParams().id;
  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      const res = await axios(
        `https://api.themoviedb.org/3/movie/${id}?api_key=8bdfe857f0bc2c006c618bf788f71f61`
      );
      setData(res.data);
      console.log(res.data);
    };
    fetchData();
  }, [id]);

  return (
    <WrapperItems>
      <ButtonBack onClick={() => history.goBack()}>
        <Back></Back>
      </ButtonBack>
      <Item key={data.id} style={{ backgroundImage: `url(https://image.tmdb.org/t/p/w300/${data.poster_path})` }}>
    </Item>
      <Title>{data.title}</Title>
      <Rater
                  total={10}
                  rating={data.vote_average}
                  onRate={r => console.log(r.rating)}
                />
                <WrapperP>{data.overview}</WrapperP>
    </WrapperItems>
  );
};
const WrapperP = styled.p`
color: #000;
    font-size: 15px;
    line-height: 20px;
    -ms-word-wrap: break-word;
    word-wrap: break-word;
    padding: 4px 10px;
`
const WrapperItems = styled.div`
  position: relative;
  color: #fff;
  padding-bottom: 20px;
  padding: 20px 0px;
`;
const ButtonBack = styled.button`
  position: absolute;
  left: 0px;
  top: 50px;
  width: 26px;
    height: 26px;
    z-index: 10;
    
`;
const Title = styled.h1`
  font-size: 30px;
  line-height: 36px;
  font-family: "Lato", Arial, "Helvetica Neue", Helvetica, sans-serif;
  font-weight: 700;
  line-height: 44px;
  margin-right: 10px;
  text-shadow: 0 2px 0 rgba(0, 0, 0, 0.5);
  -ms-word-wrap: break-word;
  word-wrap: break-word;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
`;
const Item = styled.div`
height: 475px;
width: 100%;
background-position: center 25%;
background-repeat: no-repeat;
position: relative;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
display: inline-block;

`;
const Back = styled(ArrowBack)`
  height: 26px;
  width: 26px;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;
`;
export default Single;
