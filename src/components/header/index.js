import React from 'react'
import Menu from '../menu/index'
import Login  from '../login/index'
import Register  from '../register/index'
import logo from '../../assets/img/sprite.png'
import styled from 'styled-components';
class Header extends React.Component {
   render() {
       return (<div>
  <WrapHeader>
  <Button></Button>
  <HeaderUser>
      <Login></Login>
      <Register></Register>
  </HeaderUser>
  <SearchBtn>
    <span></span>
  </SearchBtn>
  </WrapHeader>
              <Menu />    
              
       </div>);
   }
}

const Button = styled.a`
background: url(${logo}) -274px -104px no-repeat;
height: 39px;
width: 41px;
display: inline-block;
float: left;
`;
const WrapHeader = styled.div`
border-bottom: 1px solid #f6f6f6;
padding: 10px;
    height: 45px;
    position: relative;
    z-index: 2;
`;
const SearchBtn = styled.div`
display: block;
position: absolute;
    right: 0;
    cursor: pointer;
    top: 50%;
    transform: translateY(-50%);
span{
    background: url(${logo}) -224px -258px no-repeat;
    height: 22px;
    width: 22px;
    display: inline-block;
}
`;
const HeaderUser = styled.div`
    float: none;
    text-align: center;
    position: relative;
    top: 50%;
    transform: translateY(-50%);
`;
export default Header