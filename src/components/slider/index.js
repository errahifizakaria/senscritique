import React, { useState, useEffect } from 'react'
import styled from 'styled-components';
import axios from 'axios'
function Slider() {
    const [data, setData] = useState([])
    useEffect( () => {
        const fetchData = async () => {
            const res = await axios(
                'https://api.themoviedb.org/3/movie/popular?api_key=8bdfe857f0bc2c006c618bf788f71f61'
                )
                setData(res.data.results)
                console.log( res.data.results)
        }
        fetchData()
        //console.log( fetchData())
    }, [])
    return (
        <WrapperItems>
    <Items>
        {data.slice(1, 4).map(movie => {
            return(
                <Item key={movie.id} style={{ backgroundImage: `url(https://image.tmdb.org/t/p/w300/${movie.poster_path})` }}>
                    <ItemLink  href={`/movie/${movie.id}!`} >
                        <span>
                        {movie.title}
                        </span>
                    </ItemLink>
                </Item>
            )
        })}
    </Items>
    </WrapperItems>
    )
}
const WrapperItems = styled.div`
margin-bottom: 0;
overflow-x: hidden;
width: 100%;
position: relative;
`
const Items = styled.ul`
line-height: 0;
overflow: hidden;
white-space: nowrap;
list-style-type: none;
padding:0px;
margin:0px;
`
const Item = styled.li`
height: 270px;
width: 100%;
background-position: center 25%;
background-repeat: no-repeat;
position: relative;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
display: inline-block;

`
const ItemLink = styled.a`
display: flex;
    justify-content: center;
    align-items: center;
    white-space: normal;
    background-color: rgba(0,0,0,.2);
    bottom: 0;
    left: 0;
    position: absolute;
    right: 0;
    text-align: center;
    top: 0;
    z-index: 1;
    text-decoration: none;
    span{
    width: 90%;
    display: block;
    font-size: 24px;
    line-height: 1.3;
    color: #fff;
    display: block;
    font-weight:500;
    text-shadow: 0 2px 2px #000;
    }
`
export default Slider