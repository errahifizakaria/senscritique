import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";

class Menu extends React.Component {
  render() {
    return (
      <div>
        <List>
          <Item> 
            <a href="/films"> Films </a>
          </Item>
          <Item> 
            <a href="/series"> Séries </a>
          </Item>
          <Item> 
            <a href="/jeuxvideo"> Jeux </a>
          </Item>
          <Item> 
            <a href="/livres"> Livres </a>
          </Item>
          <Item> 
            <a href="/bd"> BD </a>
          </Item>
          <Item> 
            <a href="/musique"> Musique </a>
          </Item>
        </List>
      </div>
    );
  }
}
const List = styled.ul`
font-size:20px;
line-height: 39px;
overflow-x: scroll;
vertical-align: middle;
color:#fff;
padding: 0px;
white-space: nowrap;
`;
const  Item = styled.li`
display:inline-block;
cursor: pointer;
text-transform: uppercase;
vertical-align: middle;
margin-left:13px;
a{
    color:#000;
    text-decoration: none;
}
`;
export default Menu;
