import React from 'react';
import Header from '../../components/header/index'
import Slider from '../../components/slider/index'
import Nowplaying from '../../components/nowplaying/index'




function Home() {
  return (
<div>

      <Header></Header>
      
<Slider></Slider>

<Nowplaying></Nowplaying>

</div>
  );
}

export default Home;