import React, { Component } from 'react'
import styled from 'styled-components';
export class Register extends Component {
    render() {
        return (
            
                 <ButtonRegister href="/"> S'inscrire </ButtonRegister>
         
        )
    }
}
const ButtonRegister = styled.a`
background-color: #0ad06f;
    border-radius: 18px;
    color: #fff;
    display: inline-block;
    font-size: 16px;
    margin-left: 10px;
    outline: 0;
    padding: 10px 20px;
    text-decoration: none;
`;
export default Register
